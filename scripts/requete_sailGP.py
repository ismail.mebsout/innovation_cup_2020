import urllib.request, json
import pandas as pd
from arrow import Arrow, utcnow
import timeit


# boats = ['AUS', 'FRA', 'CHN', 'USA', 'GBR', 'JPN']
boats = ['FRA']

start_time = '2019-08-06T09:30:00'
stop_time = '2019-08-06T20:00:00'
# stop_time = utcnow().format('YYYY-MM-DDTHH:mm:ss')

source = 'PDS'  #'RT or 'PDS'
var_list = ['_LATITUDE_deg', '_LONGITUDE_deg', '_BOAT_SPEED_kn',
            '_TWS_kn', '_TWA_deg', '_TWD_deg',
            '_HEADING_deg', '_ANGLE_CA1_deg', '_ANGLE_CA2_deg', '_ANGLE_CA3_deg',
            '_ANGLE_CA4_deg', '_LENGTH_RH_P_mm', '_LENGTH_RH_S_mm',
            '_LENGTH_RH_BOW_mm', '_ANGLE_WING_TWIST_deg', '_PER_WING_TWIST_pct',
            '_ANGLE_WING_ROT_deg', '_AWA_deg', '_AWS_kn', '_AWS_km_h_1',
            '_QD_COG_deg', '_COURSE_deg', '_PITCH_deg', '_HEEL_deg',
            '_LEEWAY_deg', '_ANGLE_DB_RAKE_P_deg', '_ANGLE_DB_RAKE_S_deg',
            '_LENGTH_DB_H_S_m', '_LENGTH_DB_H_P_m', '_ANG_RUD_AV_deg',
            '_ANG_RUD_DIFF_deg', '_ANGLE_RUD_RAKE_P_deg',
            '_ANGLE_RUD_RAKE_S_deg', '_ANGLE_FLAP_1_TWIST_deg',
            '_ANGLE_FLAP_2_TWIST_deg', '_ANGLE_FLAP_3_TWIST_deg',
            '_LENGTH_DB_H_P_mm', '_LENGTH_DB_H_S_mm', '_LENGTH_RH_P_m',
            '_LENGTH_RH_S_m', '_LENGTH_RH_BOW_m', '_OFFSET_MWA_deg', '_MWA_deg', '_MWS_km_h_1',
            '_MWS_kn', '_OFFSET_WIND_WEIGHT_pct', '_MWA_COR_deg',
            '_MD4_MWA_ZERO_unk', '_ANGLE_DB_AOA_P_deg',
            '_ANGLE_DB_AOA_S_deg', '_PER_JIB_LEAD_pct', '_ANGLE_CANT_P_deg',
            '_ANGLE_CANT_S_deg', '_M110_MODE_DB_DOWN_unk',
            '_M110_MODE_DB_UP_unk', '_M110_MODE_SWAP_unk',
            '_M110_MODE_UPWIND_unk', '_M110_MODE_STOW_unk',
            '_M110_MODE_MANEUVER_unk', '_WHEEL_MAP_SWITCH_P_unk',
            '_WHEEL_MAP_SWITCH_S_unk', '_MD4_PER_INV_UW_SPD_pct',
            '_MD4_PER_TWIST_PED_EASE_pct', '_MD4_PER_INV_DW_SPD_pct',
            '_MD4_PER_CAMBER_EASE_pct', '_MD4_PER_CAMBER_PUMP_pct',
            '_MD4_PER_TWIST_EASE_pct', '_MD4_PWM_JIB_SHT_RET_pct',
            '_MD4_PER_TWIST_PUMP_pct', '_MD4_PWM_JIB_SHT_EXT_pct',
            '_MD4_PWM_JIB_CUN_EXT_pct', '_MD4_PWM_JIB_CUN_RET_pct',
            '_MD4_PWM_JIB_LEAD_EXT_pct', '_MD4_PWM_JIB_LEAD_RET_pct',
            '_PLT_GAIN_FAST_LIFT_unk', '_PLT_GAIN_MN_unk',
            '_PLT_GAIN_RUD_DIFF_unk', '_PLT_GAIN_SL_unk'
            ]


def get_data(var, start_time, stop_time, freq_ms='1000'):
    """
    Gets data betweem times of the specified variable on the sailGP api.
    :param var: str
    :param start_time: str format '2019-06-22T00:00:00' (%Y-%m-%dT%H
    :param stop_time: str
    :return: dataframe
    """
    start_time = start_time.format('YYYY-MM-DDTHH:mm:ss')
    stop_time = stop_time.format('YYYY-MM-DDTHH:mm:ss')

    url = r'http://api.sailgp.com/api/data?measurement=' + var + '&time_min=' + start_time + '&time_max=' + stop_time + '&freq_ms='+freq_ms+'&to_sailing=false'

    try:
        with urllib.request.urlopen(url) as url:
            data = json.loads(url.read().decode())
            data = pd.DataFrame.from_dict(data['data'], orient='index')
    except urllib.error.HTTPError as err:
        if err == 404:
            print(var + ' not found in DB')
            return None
        else:
            print(var + ' raised err:' + str(err))
            return None

    return data


def concat(df1, df2, axis):
    if df1.empty:
        df1 = df2
    else:
        df1 = pd.concat([df1, df2], axis=axis, sort=False)
    return df1


def create_boat_log(boat, source, start_time, stop_time):
    start_time = Arrow.strptime(start_time, '%Y-%m-%dT%H:%M:%S')
    stop_time = Arrow.strptime(stop_time, '%Y-%m-%dT%H:%M:%S')
    log = pd.DataFrame()

    # Creating 2h intervals:
    for var in var_list:
        req_var = boat + '_' + source + var
        print('processing '+req_var+'...')
        data = get_data(req_var, start_time, stop_time)
        if data is None:
            print(req_var+' is empty')
            continue
        log = concat(log, data, axis=1)

    log.insert(0, 'DateTime', log.index)
    log.DateTime = log.DateTime.apply(lambda x: x[:-5].replace('T',' '))
    log = log.sort_index()
    log = log.dropna(subset=['LATITUDE', 'LONGITUDE'])
    log.to_csv('Day Logs/' + boat + '_' + source + '_' +
               stop_time.format('YYYY-MM-DD')+'.csv', index=False)


for b in boats:
    create_boat_log(b, source, start_time, stop_time)